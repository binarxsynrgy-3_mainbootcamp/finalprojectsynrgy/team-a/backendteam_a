package com.fundraisey.backend.entity.startup;

public enum StartupNotificationStatus {
    approved, rejected
}
