package com.fundraisey.backend.entity.startup;

public enum CredentialStatus {
    pending, accepted, rejected
}
