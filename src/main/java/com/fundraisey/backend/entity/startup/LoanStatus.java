package com.fundraisey.backend.entity.startup;

public enum LoanStatus {
    pending, accepted, rejected, withdrawn
}
