package com.fundraisey.backend.entity.investor;

public enum InvestorVerificationStatus {
    pending, approved, rejected
}
