package com.fundraisey.backend.entity.transaction;

public enum TransactionStatus {
    paid, pending, fail
}
