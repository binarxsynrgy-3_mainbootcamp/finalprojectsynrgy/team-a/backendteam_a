package com.fundraisey.backend.entity.transaction;

public enum ReturnStatus {
    paid, unpaid
}
