package com.fundraisey.backend.model;

import lombok.Data;

@Data
public class LoanStatusModel {
    Long loanId;
}
