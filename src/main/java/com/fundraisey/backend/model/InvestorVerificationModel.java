package com.fundraisey.backend.model;

import lombok.Data;

@Data
public class InvestorVerificationModel {
    Long investorId;
}
