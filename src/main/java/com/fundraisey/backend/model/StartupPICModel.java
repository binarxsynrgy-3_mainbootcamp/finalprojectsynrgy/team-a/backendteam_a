package com.fundraisey.backend.model;

import lombok.Data;

@Data
public class StartupPICModel {
    private String picName;
    private String picPhone;
}
