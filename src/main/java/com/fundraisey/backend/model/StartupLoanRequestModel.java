package com.fundraisey.backend.model;

import lombok.Data;

@Data
public class StartupLoanRequestModel {
    Long loanId;
    Integer period;
}
