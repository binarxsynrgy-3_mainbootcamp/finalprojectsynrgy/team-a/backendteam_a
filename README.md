# FundRaisey

## Description
FundRaisey is a peer-to-peer investment platform for startups to fund their projects and investors to manage their idle money in a practical yet secure way

## Video
[Demo Video](https://drive.google.com/file/d/12jD73G_w3TiYfUH21b2Ni2lU1n8Y64ad/view)

## Documentation
[Postman Documentation](https://documenter.getpostman.com/view/8725786/UV5f8ZdP)
